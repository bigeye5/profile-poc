import Vue from 'vue';
import Router from 'vue-router';
import Logon from '../pages/Logon.vue';
import NotFound from '../pages/NotFound.vue';
import RedLayout from '../components/portals/red/RedLayout.vue';
import RedHome from '../pages/portals/red/Home.vue';
import AnotherPage from '../pages/portals/red/AnotherPage.vue';
import GreenLayout from '../components/portals/green/GreenLayout.vue';
import GreenHome from '../pages/portals/green/Home.vue';
import BlueLayout from '../components/portals/blue/BlueLayout.vue';
import BlueHome from '../pages/portals/blue/Home.vue';

Vue.use(Router);
const router = new Router({
  mode: 'history',
  routes: [
    { path: '/', component: Logon },
    {
      path: '/red',
      component: RedLayout,
      children: [
        { path: 'home', component: RedHome },
        { path: 'another', component: AnotherPage }
      ]
    },
    {
      path: '/green',
      component: GreenLayout,
      children: [{ path: 'home', component: GreenHome }]
    },
    {
      path: '/blue',
      component: BlueLayout,
      children: [{ path: 'home', component: BlueHome }]
    },
    { path: '*', component: NotFound }
  ]
});

export default router;
